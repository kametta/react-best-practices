# Audience

React is suitable for everyone because it's a completely novel approach to web development.

If one already knows HTML, CSS it might help, but it doesn't offer an outstanding advantage. Components and design systems hide these abstractions and incorporate the necessary knowledge to build the front-end. 

Programming experience helps only if it's functional. In React no one writes complex algorithms; instead manipulates the data flow using functional programming concepts.

The knowledge of older paradigms like MVC, REST and CRUD doesn't helps either. The React ecosystem -- GraphQL and Relay -- sports concepts unseen previously in web frameworks.

In the next chapters, learning React through best practices, one will find out gradually: 

1. React is not just another web framework.
2. React is foremost functional and reactive.
3. It comes together with a novel full-stack ecosystem.
4. React apps are best written using Facebook's original guidelines.
