# React Best Practices

## Introduction

- [[introduction]] - free form version
- [[introduction-1]] - more disciplined version

## Audience
- [[audience]]

## The Facebook stack
- [[the-facebook-stack]]

## The Facebook way
- [[the-facebook-way]]

## Thinking in React
- [[thinking-in-react]]

## Functional and Reactive programming
- [[functional-and-reactive-programming]]

## Event-driven architecture
- [[event-driven-architecture]]

## Data-driven applications
- [[data-driven-applications]]

## Domain-driven design
- [[domain-driven-design]]

## Design systems
- [[design-systems]]

## Formal verification (aka Testing)
- [[formal-verification]]

## React and AWS
- [[aws]]

## Open source packages
- [[awesome-react]]


## Glossary

